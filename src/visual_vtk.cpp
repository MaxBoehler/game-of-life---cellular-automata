#include "visual_vtk.h"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

static FILE *vtkFile = nullptr;

static void force_big_endian(unsigned char *bytes)
{
    static int doneTest = 0;
    static int shouldSwap = 0;
    if (!doneTest)
    {
        int tmp1 = 1;
        unsigned char *tmp2 = (unsigned char *) &tmp1;
        if (*tmp2 != 0)
            shouldSwap = 1;
        doneTest = 1;
    }

    if (shouldSwap)
    {
        unsigned char tmp = bytes[0];
        bytes[0] = bytes[3];
        bytes[3] = tmp;
        tmp = bytes[1];
        bytes[1] = bytes[2];
        bytes[2] = tmp;
    }
}

static void write_int_binary(int val)
{
  force_big_endian((unsigned char *) &val);
  fwrite(&val, sizeof(int), 1, vtkFile);
}

static void write_string(const char *str)
{
    fprintf(vtkFile,"%s", str);
}


static void close_file(void)
{
    fclose(vtkFile);
    vtkFile = nullptr;
}



void writeVTK(int x, int y, std::vector<int> game_array, int genCounter)
{
  char  str[128];

  char * filename;
  sprintf(filename, "vtk/gol_%d.vtk", genCounter);

  vtkFile = fopen(filename, "w+");

  write_string("# vtk DataFile Version 3.0\n");
  write_string("vtk output\n");
  write_string("BINARY\n");
  write_string("DATASET RECTILINEAR_GRID\n");
  sprintf(str, "DIMENSIONS %d %d %d\n", x+1, y+1, 1);
  write_string(str);
  sprintf(str, "X_COORDINATES %d integer\n", x+1);
  write_string(str);
  for (int i = 0; i < x + 1; i++)
  {
    write_int_binary(i);
  }
  sprintf(str, "Y_COORDINATES %d integer\n", y+1);
  write_string(str);
  for (int i = 0; i < y + 1; i++)
  {
    write_int_binary(i);
  }
  write_string("Z_COORDINATES 1 integer\n");
  write_int_binary(0);
  sprintf(str, "CELL_DATA %d\n", x*y);
  write_string(str);
  write_string("FIELD FieldData 1\n");
  sprintf(str, "cellscalar 1 %d integer\n", x*y);
  write_string(str);
  for(int i = 0; i < x; ++i)
  {
    for(int j = 0; j < y; ++j)
    {
      write_int_binary(game_array[i*y+j]);
    }
  }
  write_string("\n");
  sprintf(str, "POINT_DATA %d",(x+1)*(y+1));
  write_string(str);
  close_file();
}
