#include "core.h"
#include "visual_vtk.h"
#include <iostream>
#include <random>
#include <omp.h>

GameOfLife::GameOfLife(int X, int Y, int LIM, double SE)
{
  x = X;
  y = Y;
  limit = LIM;
  seed = SE;
  genCounter = 1;
  for(int i = 0; i < x; ++i)
  {
    for(int j = 0; j < y; ++j)
    {
      game_array.push_back(0);
    }
  }
}

void GameOfLife::RunSimulation()
{
  initField();
  while (genCounter <= limit)
  {
    writeVTK(x,y,game_array, genCounter);
    std::cout << "Generation " << genCounter << " finished!" << "\n";
    NextGen();
    genCounter++;
  }
}

void GameOfLife::parameters()
{
  std::cout << "Rows  = " << x << "\n";
  std::cout << "Cols  = " << y << "\n";
  std::cout << "Limit = " << limit << "\n";
  std::cout << "Seed  = " << seed << "\n";
}

double GameOfLife::RndNbr()
{
   // Generate random Number between 0 and 1 as seen in https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
   std::random_device rd;
   std::mt19937 gen(rd());
   std::uniform_real_distribution<> dis(0.0, 1.0);

   return dis(gen);
}

void GameOfLife::initField()
{
  #pragma omp parallel for collapse(2)
  for(int i = 0; i < x; ++i)
  {
    for(int j = 0; j < y; ++j)
    {
      double rnd = RndNbr();
      if (rnd <= seed) {
        game_array[i*y+j] = 1;
      }
    }
  }
}

void GameOfLife::NextGen()
{
  game_array_old = game_array;

  #pragma omp parallel for
  for(int i = 0; i < x; ++i)
  {
    for(int j = 0; j < y; ++j)
    {
      int nghbr_counter = CountNeighbours(i,j,game_array_old);
      if (game_array_old[i*y+j] == 1 && nghbr_counter < 2) { game_array[i*y+j] = 0; }
      else if (game_array_old[i*y+j] == 1 && (nghbr_counter == 2 || nghbr_counter == 3)) { continue; }
      else if (game_array_old[i*y+j] == 1 && nghbr_counter > 3) { game_array[i*y+j] = 0; }
      else if (game_array_old[i*y+j] == 0 && nghbr_counter == 3) { game_array[i*y+j] = 1; }
    }
  }
}

int GameOfLife::CountNeighbours(int i, int j, std::vector<int> game_array_old)
{
  int nghbr_counter{0};
  if (game_array_old[((i-1) % y)*y+((j+1) % x)] == 1) {nghbr_counter++;}
  if (game_array_old[((i-1) % y)*y+((j) % x)] == 1) {nghbr_counter++;}
  if (game_array_old[((i-1) % y)*y+((j-1) % x)] == 1) {nghbr_counter++;}
  if (game_array_old[(i % y)*y+((j-1) % x)] == 1) {nghbr_counter++;}
  if (game_array_old[((i+1) % y)*y+((j-1) % x)] == 1) {nghbr_counter++;}
  if (game_array_old[((i+1) % y)*y+((j) % x)] == 1) {nghbr_counter++;}
  if (game_array_old[((i+1) % y)*y+((j+1) % x)] == 1) {nghbr_counter++;}
  if (game_array_old[(i % y)*y+((j+1) % x)] == 1) {nghbr_counter++;}
  return nghbr_counter;
}


void GameOfLife::print_field()
{
  for(int i = 0; i < x; ++i)
  {
    for(int j = 0; j < y; ++j)
    {
      std::cout << game_array[i*y+j] << " ";
    }
    std::cout << "\n";
  }
}
