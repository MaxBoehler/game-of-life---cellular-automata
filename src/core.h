#include <vector>

class GameOfLife
{
  public:
    GameOfLife(int X = 250, int Y = 250, int LIM = 1500, double SE = 0.1);
    void parameters();
    void RunSimulation();
  private:
    int x, y, limit, genCounter;
    std::vector<int> game_array;
    std::vector<int> game_array_old;
    double seed;
    double RndNbr();
    void initField();
    void print_field();
    void NextGen();
    int CountNeighbours(int i, int j, std::vector<int> game_array_old);
};
